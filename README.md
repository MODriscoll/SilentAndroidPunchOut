<p>A 1v1 multiplayer game where you go head-to-head with your opponent in a best out of 7.
Use your shield to block incoming attacks while striking your own.</p>

<h1><b>This project was developed by:</b></h1>

<b>Jonathan Coleman</b> - Designer 
<p>Linked-In: https://www.linkedin.com/in/jonathan-coleman-1b8789175/</p>

<b>Michael O'Driscoll</b> - Programmer 
<p>Linked-In: https://www.linkedin.com/in/modriscoll55/ </p>

<b>Darien Ash-Rose</b> - Environmental Artist 
<p></p>

<b>Joshua Yong</b> - Modelling, Rigging, Animating Artist 
<p>Art Station: https://www.artstation.com/joshuazyong</p>
<p>Linked-In: https://www.linkedin.com/in/joshua-yong-470412154/</p>